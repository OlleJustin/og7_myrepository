
public class Mannschaftsleiter extends Mitglied {
	/**Attribute **/
	private String nameMannschaft;
	private int rabatt;

	/**Konstruktoren **/

	public Mannschaftsleiter(String name, String telefonnr, boolean jahresbeitragGezahlt, String nameMannschaft, int rabatt) {
		super(name, telefonnr, jahresbeitragGezahlt);
		this.nameMannschaft = nameMannschaft;
		this.rabatt = rabatt;
	}
	
	/**Methoden  **/
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
}
