
public class Schiedsrichter extends Mitglied{
	
	/**Attribute **/
	private String name;
	private int anzahlSpiele;
	
	/**Konstruktoren **/

	
	public Schiedsrichter(String name, int anzahlSpiele, String telefonnr, boolean jahresbeitragGezahlt) {
		super(name, telefonnr, jahresbeitragGezahlt);
		this.anzahlSpiele = anzahlSpiele;
	}
	/**Methoden  **/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}
	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}

}
