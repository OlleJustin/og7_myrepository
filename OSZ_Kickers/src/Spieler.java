
public class Spieler extends Mitglied{
	/**Attribute **/
	private int trikotnummer;
	private String spielposition;
	
	/**Konstruktoren **/

	public Spieler(String name, String telefonnr, boolean jahresbeitragGezahlt, int trikotnummer, String spielposition) {
		super(name, telefonnr, jahresbeitragGezahlt);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
		
	}
	/**Methoden  **/
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getSpielposition() {
		return spielposition;
	}
	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}
}
