
public class Mitglied {
	/**Attribute **/
	private String name;
	private String telefonnr;
	private boolean jahresbeitragGezahlt;

	/**Konstruktoren **/
	
	public Mitglied(String name, String telefonnr, boolean jahresbeitragGezahlt) {
		super();
		this.name = name;
		this.telefonnr = telefonnr;
		this.jahresbeitragGezahlt = jahresbeitragGezahlt;
	}
	
	/**Methoden  **/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnr() {
		return telefonnr;
	}
	public void setTelefonnr(String telefonnr) {
		this.telefonnr = telefonnr;
	}

	public boolean isJahresbeitragGezahlt() {
		return jahresbeitragGezahlt;
	}

	public void setJahresbeitragGezahlt(boolean jahresbeitragGezahlt) {
		this.jahresbeitragGezahlt = jahresbeitragGezahlt;
	}
	
}
