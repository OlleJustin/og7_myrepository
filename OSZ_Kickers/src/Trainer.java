
public class Trainer extends Mitglied {
	/**Attribute **/
	private String lizenzklasse;
	private double aufwandentschaedigung;

	/**Konstruktoren **/

	
	public Trainer(String name, String telefonnr, boolean jahresbeitragGezahlt, String lizenzklasse, double aufwandentschaedigung) {
		super(name, telefonnr, jahresbeitragGezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
	/**Methoden  **/
	public String getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(String lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public double getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}
	public void setAufwandentschaedigung(double aufwandentschaedigung) {
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
}
