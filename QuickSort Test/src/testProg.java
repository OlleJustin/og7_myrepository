
import java.util.Random;

public class testProg
{
   public static final int MAX = 50000;

   public static void main( String[] args )
   {
      double[] x = new double[MAX];
      double[] tmp = new double[MAX];
      Random   r = new Random(1234);

      for ( int i = 0; i < MAX; i++ )
         x[i] = r.nextDouble();

      System.out.println("Start sorting....");

      long startTime = System.currentTimeMillis();

      QuickSort.sort( x, 0, x.length );  // Quicksort

      long stopTime = System.currentTimeMillis();
      long elapsedTime = stopTime - startTime;
      System.out.println("Elasped time = " + elapsedTime + " msec");

      for ( int i = 0; i < 4; i++ )
         System.out.print(x[i] + " ");
   }
}
