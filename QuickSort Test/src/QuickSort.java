import java.util.Arrays;


public class QuickSort
{
   public static int indent = 0;

   /* =============================================================
      
                      smaller_I         larger_I
                          |                |
                          V                V
             +----------+---+------------+---+---------+
             | <= pivot |   |            |   | > pivot |
             +----------+---+------------+---+---------+
                                           ^
                                          "hole"

      ============================================================= */

   public static int partition( double[] a, int iLeft, int iRight )
   {
      double   pivot;
      double   help;
      int      smaller_I, larger_I;

      smaller_I = iLeft;
      larger_I  = iRight-1;
      pivot = a[iRight-1];     // Hole is initially at a[iRight-1]

      while ( larger_I > smaller_I )
      {
         if ( a[larger_I -1] > pivot )
         {  /* ==================================================
               Put a[larger_I -1] in the hole and move hole down
               ================================================== */
            a[larger_I] = a[larger_I -1];
	    larger_I--;
         }
	 else
         {
            /* =================================================
               a[larger_I -1] <= pivot, so:
	          Swap a[smaller_I] with a[larger_I -1] and
		  move smaller_I up
               ================================================= */
             help = a[larger_I -1];
             a[larger_I -1] = a[smaller_I];
	     a[smaller_I] = help;
	     smaller_I++;
         }
      }

      a[larger_I] = pivot;   // Put pivot in the "hole"

      return larger_I;
   }


   public static void sort( double[] a, int iLeft, int iRight )
   {
      int      i, j, k;

      if ( iRight - iLeft <= 1 )
      {
         // Array of 1 element is sorted....
         return;
      }

      k = partition( a, iLeft, iRight ); // k = position of pivot 
                                         //     after partitioning
      /* =================================================
         Sort each half
         ================================================= */
      sort(a, iLeft, k);
      sort(a, k+1, iRight);

      /* =================================================
         Concatenate the pieces back is NOT necessary !
         ================================================= */
   }

}
