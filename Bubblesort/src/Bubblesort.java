public class Bubblesort {
 
    public static int[] sortiere(int[] liste) {
        boolean wirHabenNichtMehrGetauscht;
         
        do{
            wirHabenNichtMehrGetauscht = true;
             
            for(int i=0; i<liste.length; i++){
                // Laufen wir auch nicht zu weit?
                boolean nichtAnGrenze = (i+1 < liste.length);
                 
                if(nichtAnGrenze){
                    // Wir greifen nicht auf Feld "31" zu
                    boolean werteVertauscht = (liste[i] > liste[i+1]);
              
                     
                    if(werteVertauscht){
                        // Wir m�ssen tauschen
                        int temp = liste[i];
                         
                        liste[i] = liste[i+1];
                        liste[i+1] = temp;
                         
                        // Liste war noch nicht sortiert
                        wirHabenNichtMehrGetauscht = false; 
                    }
                }
 
            }
        }while(!wirHabenNichtMehrGetauscht);
         
        return liste;
    }
 
}