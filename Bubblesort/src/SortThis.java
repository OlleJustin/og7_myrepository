public class SortThis {
 
    public static void main(String[] args) {
        // Erstelle Array
        int [] liste = new int[30];
         
        // F�lle Array mit Zufallszahlen
        for(int i=0; i < 30; i++){
            liste[i] = (int) (Math.random() * 100);
        }
         
        // Gebe unsortierte Liste aus
        System.out.println("Unsortiert:");
        for(int i=0; i < 30; i++){
            System.out.print(liste[i]+ " | " );
        }
         
        // Sortiere mit BubbleSort
        Bubblesort.sortiere(liste);
 
        // Gebe sortierte Liste aus
        System.out.println("Sortiert:");
        for(int i=0; i < 30; i++){
            System.out.print(liste[i]+ "| " );
        }
    }
 
}