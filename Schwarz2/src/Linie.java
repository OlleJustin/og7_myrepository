import java.io.File;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class Linie {

	static int searchLineWiederholungen = 0;
	static LightSensor lichtsensor = new LightSensor(SensorPort.S1);
	static final int NUMBER_OF_MENU_POINTS = 2;
	static final int START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN = 2;
	static final int CONSTANT_SPEED = 140;
	static final int MAX_VAR_SPEED = 30;
	static final double CONSTANT_DREHUNG = 3.0;
	static final double MAX_VAR_DREHUNG = 1.5;

	public static void main(String[] args) {
		int currentMenuPoint = 1;
		String[] menuPointDesciption = {"", ""};
		LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
		do {
			if (Button.LEFT.isPressed()) {
				if (currentMenuPoint > 1) {
					currentMenuPoint--;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
					do {
						
					} while (Button.LEFT.isDown());
				}
			}
			if (Button.RIGHT.isPressed()) {
				if (currentMenuPoint < NUMBER_OF_MENU_POINTS) {
					currentMenuPoint++;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
					do {
						
					} while (Button.RIGHT.isDown());
				}
			}
		} while (!Button.ENTER.isPressed());
		
		Button.ENTER.waitForPressAndRelease();
		lichtsensor.calibrateHigh();
		Button.ENTER.waitForPressAndRelease();
		lichtsensor.calibrateLow();
		
		LCD.drawString("jA!", 0, 0);
		lichtsensor.setFloodlight(true);
		if (currentMenuPoint == 1) {
			test();
		} else if (currentMenuPoint == 2) {
			followBorderOfPath();
		}
		stop();
		Sound.beep();
	}
	
	public static void test() {
		boolean end = false;
		do {
			if (lichtsensor.getLightValue() < 50) {
				Motor.A.forward();
				Motor.C.forward();
				Motor.A.setSpeed(270);
				Motor.C.setSpeed(270);
			} else {
				stop();
				searchLineWiederholungen = START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN;
				if (searchLine(true) == false) {
					end = true;
				}
			}
		} while (!end);
	}
	
	public static boolean searchLine(boolean richtung) {
		searchLineWiederholungen++;
		if (searchLineWiederholungen > 90) {
			return false;
		} else {
			if (richtung) {
				for (int i = 0; i < searchLineWiederholungen; i++) {
					drehenOnThisVeryPoint(5);
					if (lichtsensor.getLightValue() < 50) {
						return true;
					}
				}
			} else {
				for (int i = 0; i < searchLineWiederholungen; i++) {
					drehenOnThisVeryPoint(-5);
					if (lichtsensor.getLightValue() < 50) {
						return true;
					}
				}
			}

			if (lichtsensor.getLightValue() < 50) {
				return true;
			} else {
				if (richtung) {
					drehenOnThisVeryPoint(-5*searchLineWiederholungen);
				} else {
					drehenOnThisVeryPoint(5*searchLineWiederholungen);
				}
				if (lichtsensor.getLightValue() < 50) {
					return true;
				} else return searchLine(!richtung);
			}
		}
	}
	
	public static void followBorderOfPath() {
		int ticsSinceLastDarkGround = 0;
		int ticsSinceLastWhiteGround = 0;
		int varSpeed = 0;
		double varDrehung = 0.0;
		if (lichtsensor.getLightValue() < 50) {
			Motor.A.forward();
			Motor.C.forward();
			do {
				if (lichtsensor.getLightValue() < 50) {
					ticsSinceLastDarkGround = 0;
					ticsSinceLastWhiteGround++;
					varSpeed = MAX_VAR_SPEED - (ticsSinceLastWhiteGround/5);
					if (varSpeed < 0) {
						varSpeed = 0;
					}
					varDrehung = MAX_VAR_DREHUNG - (ticsSinceLastWhiteGround/5000.0);
					if (varDrehung < 0) {
						varDrehung = 0;
					}
					Motor.A.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*0.1));
					Motor.C.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*(CONSTANT_DREHUNG + varDrehung)));
				} else {
					ticsSinceLastWhiteGround = 0;
					ticsSinceLastDarkGround++;
					LCD.clear();
					LCD.drawInt(ticsSinceLastDarkGround, 0, 0);
					varSpeed = MAX_VAR_SPEED - (ticsSinceLastDarkGround/10);
					if (varSpeed < 0) {
						varSpeed = 0;
					}
					varDrehung = MAX_VAR_DREHUNG - (ticsSinceLastDarkGround/5000.0);
					if (varDrehung < 0) {
						varDrehung = 0;
					}
					Motor.A.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*(CONSTANT_DREHUNG + varDrehung)));
					Motor.C.setSpeed(CONSTANT_SPEED + varSpeed);
				}
			} while (true);
		} else {
			searchLineWiederholungen = START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN;
			if (searchLine(true) == false) {
				stop();
			} else followBorderOfPath();
		}
	}

	public static void warten(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}

	public static void drehenOnThisVeryPoint(int gradNachRechts) {
		stop();
		Motor.A.setSpeed(270);
		Motor.C.setSpeed(270);
		if (gradNachRechts < 0) {
			Motor.A.forward();
			Motor.C.backward();
		} else {
			Motor.C.forward();
			Motor.A.backward();
		}
		warten(480 * Math.abs(gradNachRechts) / 90);
		stop();
	}
}

