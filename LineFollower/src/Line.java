import lejos.nxt.Button;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;

public class Line {
	private static LightSensor lightSensor = new LightSensor(SensorPort.S3);
	private static int links = 0;
	private static int rechts = 0;
	private static int degi = 15;

	public static void main(String[] args) {

		Button.ENTER.waitForPressAndRelease();
		lightSensor.calibrateHigh();

		Button.ENTER.waitForPressAndRelease();
		lightSensor.calibrateLow();
		kreis();

	}

	private static boolean Search(int grad, NXTRegulatedMotor motor, NXTRegulatedMotor motor2) {
		try {
			drehe();
			while (motor.isMoving()) {
				if (lightSensor.getLightValue() < 50) {
					throw new Exception();
				}
			}

			drehe();
			return false;
		} catch (Exception e) {
			motor.stop();
			return true;
		}

	}

	private static void kreis() {
		setSpeed(360);
		forward();
		while (lightSensor.getLightValue() < 50) {
		}
		stop();
		if (links > rechts) {
			if (Search(60, Motor.A, Motor.C)) {
				links++;
				kreis();
			}

			if (Search(60, Motor.C, Motor.A)) {
				rechts++;
				kreis();
			}
		} else {
			if (Search(60, Motor.C, Motor.A)) {
				rechts++;
				kreis();
			}
			if (Search(60, Motor.A, Motor.C)) {
				links++;
				kreis();
			}
		}
	}

	private static void setSpeed(int speed) {
		Motor.A.setSpeed(speed);
		Motor.C.setSpeed(speed);
	}

	private static void forward() {
		Motor.A.forward();
		Motor.C.forward();
	}

	private static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}
	
	private static void drehe() {
		int rnd = (int) (Math.random()*360)-180;
		Motor.A.rotate(rnd,true);
		Motor.C.rotate(rnd * -1, false);
		}

}
