import lejos.nxt.*;

public class LineFollower {
	
	public static LightSensor lightSensor = new LightSensor(SensorPort.S3);
	public static int Uhrzeigersinn = 0;
	public static int GegenUhrzeigersinn = 0;
	
	public static boolean Suche(int grad, NXTRegulatedMotor motor, NXTRegulatedMotor motor2) {
	try {
		motor.rotate(grad*4,true);
		while(motor.isMoving()) {
			if (lightSensor.getLightValue() < 40) {
				throw new Exception();
			}
		}
		
		motor.rotate(grad*-4);
		return false;
	} catch (Exception e) {
		motor.stop();
		return true;
	}

}
	
	public static void kreisFahren() {
		setSpeed(360);
		forward();
		while(lightSensor.getLightValue() < 40) {	
		stop();
		}
		if(Uhrzeigersinn < GegenUhrzeigersinn) {
			if (Suche(60,Motor.A, Motor.C)) {
				Uhrzeigersinn++;
				kreisFahren();
			}
			
			if(Suche(60, Motor.A, Motor.C)) {
				Uhrzeigersinn++;
				kreisFahren();
				
			}
				if (Suche(60,Motor.C,Motor.A)) {
					GegenUhrzeigersinn++;
					kreisFahren();
				} else {
				if (Suche(60, Motor.A,Motor.C)) {
					Uhrzeigersinn++;
					kreisFahren();
				}
				}
		}
		}
		
	
	
		
	
	
		
		public static void setSpeed(int speed) {
			Motor.A.setSpeed(speed);
			Motor.C.setSpeed(speed);
		}
		
		public static void forward() {
			Motor.A.forward();
			Motor.C.forward();
		}
		
		public static void stop() {
			Motor.A.stop(true);
			Motor.C.stop();
		}
		

	
	
	public static void main (String[] args) {
		
		Button.ENTER.waitForPressAndRelease();
		lightSensor.calibrateHigh();
		
		Button.ENTER.waitForPressAndRelease();
		lightSensor.calibrateLow();
		
		
		
		kreisFahren();
		try {
			Thread.sleep(10000);
			} catch (InterruptedException e) {}
	}


	
	
		
	}
	
	
	

	

