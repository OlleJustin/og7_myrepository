package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;

import java.awt.GridLayout;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;

public class BunteRechteckeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private BunteRechteckeController ctr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BunteRechteckeGUI frame = new BunteRechteckeGUI(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckeGUI(BunteRechteckeController brc) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JTextPane txtpnX = new JTextPane();
		txtpnX.setText("X");
		contentPane.add(txtpnX);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);
		
		JTextPane txtpnY = new JTextPane();
		txtpnY.setText("Y");
		contentPane.add(txtpnY);
		
		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JTextPane txtpnBreite = new JTextPane();
		txtpnBreite.setText("Breite");
		contentPane.add(txtpnBreite);
		
		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JTextPane txtpnHhe = new JTextPane();
		txtpnHhe.setText("H\u00F6he");
		contentPane.add(txtpnHhe);
		
		textField_3 = new JTextField();
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnSpeichern = new JButton("Speichern");
		contentPane.add(btnSpeichern);
		
		this.ctr = brc;
		
		JButton btnHinzufugen = new JButton("Hinzufügen");
		btnHinzufugen.addMouseListener(new MouseAdapter() {
			@Overridea
			public void mouseClicked(MouseEvent e) {
				int x =  Integer.parseInt(txtpnX.getText());
				int y =  Integer.parseInt(txtpnY.getText());
				int hoehe = Integer.parseInt(txtpnHhe.getText());
				int breite = Integer.parseInt(txtpnBreite.getText());
				ctr.add(rechteck);
			}
		});
		contentPane.add(btnHinzufugen);
	}

}
