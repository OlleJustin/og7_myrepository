package controller;

import model.MySQLDatabase;
import model.Rechteck;
import view.BunteRechteckeGUI;

import java.util.LinkedList;
import java.util.List;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		super();
		this.rechtecke=new LinkedList();
		this.database = new MySQLDatabase();
	}
	public List getRechtecke(){
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteck_eintragen(rechteck);
	}
	

	
	public void reset() {
		rechtecke.clear();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufuegen() {
	new BunteRechteckeGUI(this);
		
	}
}
