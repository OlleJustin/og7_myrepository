package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();

			 if (fuellstand<0) {
			 fuellstand = 0;
			 }
			 fuellstand = fuellstand + 5;
			 if (fuellstand>=100) {
			 fuellstand = 100;
			 }
			 f.myTank.setFuellstand(fuellstand);
			 float percentage;
			 percentage = (float)((fuellstand/100.0)*100);
			 f.lblFuellstand.setText(percentage+"% / "+fuellstand+"l");
		}
			 
		if (obj == f.btnVerbrauchen) {
		
			double fuellstand = f.myTank.getFuellstand();
			 if (fuellstand>=100) {
			 fuellstand = 100;
			 }

			fuellstand = fuellstand -2;
			if (fuellstand<=0) {
				 fuellstand = 0;
				 }
			f.myTank.setFuellstand(fuellstand);
			float percentage;
			 percentage = (float)((fuellstand/100.0)*100);
			 f.lblFuellstand.setText(percentage+"% / "+fuellstand+"l");
		}
		
		if(obj == f.btnZurücksetzen) {
			double fuellstand = f.myTank.getFuellstand();
					fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			float percentage;
			 percentage = (float)((fuellstand/100.0)*100);
			 f.lblFuellstand.setText(percentage+"% / "+fuellstand+"l");
		}

	}
}