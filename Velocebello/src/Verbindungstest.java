import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Verbindungstest {

	public static void main(String[] args) {
		try {
			// Parameter f�r Verbindungsaufbau definieren
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/test";
			String user = "root";
			String password = "";
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM t_titel");
			// Ergebnis abfragen
			while (rs.next()) {
				System.out.println(rs.getString("name"));
			}
			// Verbindung schlie�en
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
	}
}
