import java.util.LinkedList;
import java.util.List;

/**
 * Klasse die nacheinander Einheiten f�r die EinheitenkaufGUI bereitstellt
 * @author Tim Tenbusch
 * @version 28.7.2016
 */
public class Einheitenspeicher {

	private static List<Einheit> units = new LinkedList<Einheit>();
	private static int currentUnit = 0;
	
	//privater Konstruktor verhindert, dass jemand diese Klasse instanziieren kann, auch nicht n�tig da static
	private Einheitenspeicher(){}
	
	/**
	 * gibt aus einer Liste die n�chste Einheit aus, ist die Liste am Ende angelangt f�ngt sie wieder von vorne an
	 * @return die aktuelle Einheit
	 */
	public static Einheit next() {
		//Liste wird initialisiert - passiert ausschlie�lich bei dem ersten Aufruf
		if(units.size() == 0)
			fillUnitList();
		
		//wenn das Ende der Liste erreicht wurde, dann springt der Zeiger wieder auf die erste Einheit
		if (currentUnit >= units.size())
			currentUnit = 0;
		
		//R�ckgabe der aktuellen Einheit
		return units.get(currentUnit++);
	}
	
	private static void fillUnitList() {
		Einheit barbar = new Einheit("Barbar", 1, "./src/Barbar.png");
		units.add(barbar);
		Einheit drachenbaby = new Einheit("Drachenbaby", 4, "./src/Drachenbaby.png");
		units.add(drachenbaby);
		Einheit pekka = new Einheit("P.E.K.K.A", 7, "./src/P.E.K.K.A.png");
		units.add(pekka);
		//neue Einheit hinzuf�gen
		//Einheit whatever = new Einheit("Name", 0, "./src/Name.png");
		//units.add(whatever);
		
	}

}
