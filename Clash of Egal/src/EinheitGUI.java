import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;

public class EinheitGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EinheitGUI frame = new EinheitGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EinheitGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JLabel lbl_head = new JLabel("Clash Of Egal");
		lbl_head.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lbl_head.setToolTipText("");
		lbl_head.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_head, BorderLayout.NORTH);
		
		JButton btn_next = new JButton("NEXT");
		btn_next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				
				
				
			}
		});
		contentPane.add(btn_next, BorderLayout.EAST);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lbl_bild = new JLabel("New label");
		lbl_bild.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_bild);
		
		JLabel lbl_name = new JLabel("Barbar");
		lbl_name.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_name);
		
		JLabel lbl_kosten = new JLabel("4");
		lbl_kosten.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_kosten);
		
		JButton btn_kaufen = new JButton("Kaufen");
		btn_kaufen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btn_kaufen);
	}

}
