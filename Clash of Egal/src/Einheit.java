
public class Einheit {

	//Attribute ANFANG
	private String name;
	private int kosten;
	private String bildpfad;
	//Attribute ENDE
	
	//Constructor ANFANG
	public Einheit(String name, int kosten, String bildpfad) {
		this.name =  name;
		this.kosten = kosten;
		this.bildpfad = bildpfad;
	}
	//Constructor ENDE

	
	//Getter & Setter ANFANG
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKosten() {
		return kosten;
	}

	public void setKosten(int kosten) {
		this.kosten = kosten;
	}

	public String getBildpfad() {
		return bildpfad;
	}

	public void setBildpfad(String bildpfad) {
		this.bildpfad = bildpfad;
	}
	//Getter & Setter ENDE

	//Methoden ANFANG
	@Override
	public String toString() {
		return "Einheit [name=" + name + ", kosten=" + kosten + ", bildpfad=" + bildpfad + "]";
	}
	//Methoden ENDE
	
	
	
	
}
