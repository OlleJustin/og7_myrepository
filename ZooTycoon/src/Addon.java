
public class Addon {
private double preis;
private int id;
private String bezeichnung;
private int anz_addons;
private int max_addons;




public Addon(double preis, int id, String bezeichnung, int anz_addons, int max_addons) {
	super();
	this.preis = preis;
	this.id = id;
	this.bezeichnung = bezeichnung;
	this.anz_addons = anz_addons;
	this.max_addons = max_addons;
}

public double getPreis() {
	return preis;
}
public void setPreis(double preis) {
	this.preis = preis;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getBezeichnung() {
	return bezeichnung;
}
public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}
public int getAnz_addons() {
	return anz_addons;
}
public void setAnz_addons(int anz_addons) {
	this.anz_addons = anz_addons;
}
public int getMax_addons() {
	return max_addons;
}
public void setMax_addons(int max_addons) {
	this.max_addons = max_addons;
}



}
