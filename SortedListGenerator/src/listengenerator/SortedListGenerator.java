package listengenerator;

import java.util.Random;

public class SortedListGenerator {

	public static long[] getSortedList(int laenge){  //Die Methode getSortedList ist daf�r zust�ndig, die Zahlenliste in
		Random rand = new Random(111111);			//eine Reihenfolge / sortierte Liste zu bringen
		long[] zahlenliste = new long[laenge];		// Dies ist zwingend erforderlich um mit der Bin�ren Suche zu arbeiten.
		long naechsteZahl = 0;
		
		for(int i = 0; i < laenge; i++){		//Schleife zum sortieren
			naechsteZahl += rand.nextInt(3)+1;
			zahlenliste[i] = naechsteZahl;
		}
		
		return zahlenliste;
	}
	
	public static void main(String[] args){			//In der Main-Methode wird aus der sortierten Liste letztendlich die...
		final int ANZAHL = 20000000; //20.000.000	//...gesuchte Zahl gesucht
		
		long[] a = getSortedList(ANZAHL);		//Sortierte Liste
		long gesuchteZahl = a[ANZAHL-1];	//gesuchte Zahl
		
		long time = System.currentTimeMillis();	//Ab hier startet die Stoppuhr
		
		for(int i = 0; i < a.length; i++)	//Schleife zum suchen der gesuchten Zahl
			if(a[i] == gesuchteZahl)
				System.out.println(i);
		
		System.out.println(System.currentTimeMillis() - time + "ms"); //Hier wird die Zeit gestoppt und wird ausgegeben
		
		//for(long x: a)
		//	System.out.println(x);		
		
	}
}
