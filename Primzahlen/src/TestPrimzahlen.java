
public class TestPrimzahlen {

	public static void main(String[] args) {
		Primzahlen prim = new Primzahlen();
		Stoppuhr t = new Stoppuhr();
		t.start();
		
		long n = 10000;
		System.out.println("Primzahlen von 0 bis "+n);
		for(long i=0; i <= n;i++) {
		if(prim.istPrim(i)) {
			System.out.println(i);
		}
			
			
		}
		
		t.stopp();
		System.out.println("Benötigte Zeit zum Rechnen in Millisekunden: ");
		System.out.println(t.getMs());

	}

}
