
public class Stoppuhr {
private long start;
private long stopp;

public void start() {
	this.start=System.currentTimeMillis();
	}

public void stopp() {
	this.stopp=System.currentTimeMillis();
	}

public void reset() {

	}

public long getMs() {
	return stopp - start;
	}

public long getStart() {
	return start;
}

public void setStart(long start) {
	this.start = start;
}

public long getStopp() {
	return stopp;
}

public void setStopp(long stopp) {
	this.stopp = stopp;
}
}