public class Primzahlen {

	public static boolean istPrim(long i){
		   boolean primZahl=true;  //... solange wir keinen Teiler gefunden haben
		   int moeglicherTeiler = 2;  
		   while(moeglicherTeiler  < i){ //Teiler muss kleiner sein als Zahl selbst
		      if(i % moeglicherTeiler==0) { //Teiler gefunden -> keine Primzahl
		          primZahl=false;
		      }
		      moeglicherTeiler = moeglicherTeiler + 1;  //Teiler hochz�hlen
		   }
		   return primZahl;
		}
	public static void main(String[] argv) {
		Stoppuhr t = new Stoppuhr();
		t.start();
		 for (int number = 2; number < 20; number++) {
	            System.out.println("Die Zahl " + number + " ist eine Primzahl? " + istPrim(number));
	        }
		 t.stopp();
		 long ms = 0;
		 System.out.println(ms);
	}
}