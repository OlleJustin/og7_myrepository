import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javafx.scene.control.Button;

import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TicTacToe extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe frame = new TicTacToe();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void changeState(JButton btnNewButton) {
	if(btnNewButton.getText().equals("")){
		btnNewButton.setText("x");
	} else if (btnNewButton.getText().equals("X")) {
		btnNewButton.setText("0");
	} else if (btnNewButton.getText().equals("O")) {
		btnNewButton.setText("");
	}
	
}

	/**
	 * Create the frame.
	 */
	public TicTacToe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
	
	
		for(int i=0; i < 9;i++) {
			
			JButton btnNewButton = new JButton("");
			
			
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					changeState(btnNewButton);
					contentPane.add(btnNewButton);
				}
			});
			
		}
		

}

}

