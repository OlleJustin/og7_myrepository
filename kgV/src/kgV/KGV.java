package kgV;

import java.util.Scanner;

public class KGV {

	public static void main (String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die erste Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		System.out.println("Geben Sie die zweite Zahl ein: ");
		int zahl2 = myScanner.nextInt();
		
		int zahl3 = zahl1;
		int zahl4 = zahl2;
		
		while (zahl1!=zahl2) {
		
		if (zahl1 < zahl2) {
		zahl1 = zahl1+zahl3;
		}
		if (zahl2 < zahl1) {
		zahl2 = zahl2+zahl4;
		}
		
		}
		
		if (zahl1 == zahl2) {
		System.out.println("Das kleinste gemeinsame Vielfache ist: "+zahl1);
		}
	
	}
	
}
